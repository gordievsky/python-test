FROM python:3.8.7-alpine3.11

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip3 install -r requirements.txt
COPY . /usr/src/app

EXPOSE 8080

ENTRYPOINT ["python3"]

CMD ["main.py"]